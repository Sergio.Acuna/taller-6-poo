//REST peticiones

//Aquí se crea la API
import express from 'express';

let app = new express();

//Rutas: punto de acceso a una funcionalidad de la aplicación
function saludar(peticion, respuesta){

    respuesta.send('Hola API!!');

} //función de callback

//petición -> cliente
//respuesta -> servidor/backend

app.get('/saludo',saludar);//pasa la función como argumento

//Ruta

function calcular(req, res){

    let miSumador = new Sumador();

    let resultado = miSumador.sumar(56,78);
    //Devolver el resultado de una operación 

    res.send(resultado.toString());

}

app.get('/calculadora',calcular);

//Taller 
function aleatorio(resquest, response){

    let miAleatorio = new Aleatorio

    let resultado = miAleatorio.n_aleatorio(100,1);

    response.send('Número aleatorio: '+resultado.toString());

}

app.get('/aleatorio',aleatorio);

//Encender el servidor de la API

app.listen(3000);

class Sumador{

    sumar(a,b){

        return a+b;

    }

}

class Aleatorio{

    n_aleatorio(max, min){

        return Math.round(Math.random() * (max - min) + min);
        
    }
}